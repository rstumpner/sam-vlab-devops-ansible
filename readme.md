# SAM vLab with for Ansible
This is a Gitlab Repository to do an easy Virtual Lab Environment for Ansible ( https://www.ansible.com/ ) Open Source Software.

To Follow the Instrutions of this Virtual Lab:
Open your Browser

Option 1 (Online):
  * Browse https://gitpitch.com/rstumpner/sam-vlab-devops-ansible/master?grs=gitlab&t=simple

Option 2 (Local):
  * git clone https://gitlab.com/rstumpner/sam-vlab-devops-ansible
  * gem install showoff
  * showoff serve
  * Browse http://localhost:9090

Virtual Lab Environment Setup:

Requirements:
  * 2 GB Memory (minimal)
  * 2 x CPU Cores
  * Virtualbox

vLAB Setup:
  * client  (Ubuntu 16.04 )
  * ansible (Ubuntu 16.04 with Ansible installed)

Option 1 Vagrant (https://www.vagrantup.com/) (local):
  * Downlad and Install Vagrant Package for your OS
    * On Linux
      * wget https://releases.hashicorp.com/vagrant/2.0.1/vagrant_2.0.1_x86_64.deb?_ga=2.85169500.497796412.1511295690-1446073050.1511295690
      * dpkg -i vagrant_2.0.1_x86_64.deb
    * On Windows
        * https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.msi?_ga=2.89037278.439840422.1522128825-1814262215.1522128825
    * on macOS
        * https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.dmg?_ga=2.257941326.439840422.1522128825-1814262215.1522128825

  * Clone this Git Repository
    * git clone https://gitlab.com/rstumpner/sam-vlab-devops-ansible

  * Setup the vLAB Environment with Vagrant
    ```md
     cd sam-vlab-devops-ansible/vlab/vagrant-virtualbox/
     vagrant up
     ```
  * Check the vLAB Setup
    ```md
      vagrant status
      ```
  * Login to work with a Node
    ```md
    vagrant ssh ansible
    ```

#### Troubleshooting
