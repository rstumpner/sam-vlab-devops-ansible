<!-- $size: 16:9 -->
<!-- page_number: true -->
<!-- footer: Roland Stumpner GPLv3 -->


## Systemadministraton vLAB
#### Version 20210222
* Devops
* Konfigurationsmanagement
* Ansible

---

## Agenda
* Ansible Einführung
* Voraussetzungen vLAB
* Ansible First Steps
* Ansible Aufgaben Level 1
* Ansible Rollen
* Ansible Aufgaben Level 2

---
## Ansible Voraussetzungen vLAB
* VM ( Virtualbox / Vmware Workstation / Proxmox VM)
* 1 x CPU Core
* 3 GB RAM
* Linux Ubuntu 16.04 LTS

---
## Vorbereitungen vLAB (Vagrant)
* git clone https://www.gitlab.com/rstumpner/sam-vlab-devops-ansible
* cd vlab/vagrant-virtualbox
* vagrant up
* vagrant ssh ansible
* Username: ansible
* Password: ansible



---
## Einführung Ansible
* Ist ein Framework zum ausführen von Tasks (AD Hoc)
* Konfigurationsmanagement Tool
* Based on Python
* YAML und JSON als Basis für die Beschreibung der Konfiguration
* Client / Server Architektur
* Zur Kommunikation werden Standard Protokolle verwendet
* Zum Beispiel
    * SSH
    * WinRM
* Template Engine: Jinja2

---
## Überblick vLAB Ansible

![Architektur](_images/ansible-architektur.png)


---
## The vLAB Environment
* ansible.local
  * (Ubuntu 16.04 / Port 8083)
* client.local
  * (Ubuntu 16.04 / Port 8082)


---
## Installation ansible.local (Manual)
* Ubuntu 16.04 LTS Basis
* Install Ansible
```bash
sudo apt-get update
sudo apt-get install -y git
sudo apt-get install -y curl
sudo apt-get install -y links
sudo apt-get install -y tree
sudo apt-get install -y software-properties-common
sudo apt-add-repository ppa:ansible/ansible
sudo apt-get update
sudo apt-get install -y ansible
```


---
## vLAB Environment
#### Funktionsüberprüfung

```bash
ping localhost
ping client.local
ssh -l ansible localhost
ssh -l ansible client.local
```

---
## First Steps in Ansible
* Check Ansible Version:
	* ansible --version

```
ansible 2.3.0.0
  config file = /etc/ansible/ansible.cfg
  configured module search path = Default w/o overrides
  python version = 2.7.12 (default, Nov 19 2016, 06:48:10) [GCC 5.4.0 20160609]
```

---
## Überblick vLAB Ansible

![Architektur](_images/ansible-architektur.png)

---
## Ansible Inventory File Example (INI)
ansible-inventory:

```
[localhost]
localhost

[client]
client.local ansible_host=client.local
localhost
```
---
## Ansible Inventory File Example (YAML)
ansible-inventory.yml:
```YAML
all:
  hosts:
    localhost:
  children:
    client:
      hosts:
        client.local:
          ansible_host: client.local
```

---
## Test Ansible connection to Hosts
* Check the Local Host using Module Ping
```
ansible localhost -m ping
```
* Disable SSH Key checking
```bash
export ANSIBLE_HOST_KEY_CHECKING=false
```

* Check the Client Connection
```
ansible --inventory=/vlab-files/ansible-inventory.yml localhost -m ping -u ansible -k
```

* Check the Client Facts
```
ansible --inventory=/vlab-files/ansible-inventory.yml localhost -m setup -u ansible -k
```

---
## Ansible Adhoc Mode
* Using Ansible to Execute a Command (Module Command)

```
ansible --inventory=/vlab-files/ansible-inventory.yml localhost  -a "/bin/echo 'Managed by Ansible'" -u ansible -k
```
* Using Ansible to Execute a Command on Remote Host with Inventory File

```
ansible --inventory=/vlab-files/ansible-inventory.yml localhost  -a "/bin/ip addr" -u ansible -k
```

---
## Create a Message of the Day

```
ansible --inventory=/vlab-files/ansible-inventory.yml localhost -m lineinfile -a "path=/etc/motd line='Managed by Ansible' create=yes" -u ansible -k --become
```

---
## Sudoers and Superuser
* Set Superuser Privileges

```
echo "ansible ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/90-cloud-init-users
```

* Create a Message of the Day (clients)

```
ansible --inventory=/vlab-files/ansible-inventory.yml clients -m lineinfile -a "path=/etc/motd line='Managed by Ansible' create=yes" -u ansible -k --become
```


---
## Install Packages with a Module

```
ansible --inventory=/vlab-files/ansible-inventory.yml localhost -m apt -a "name=ntp state=present" -u ansible -k --become
```

---
## Till now a Simple CLI Tool
### Es ist aber sicher besser diese langen CLI Commandos Aufzuschreiben. Diese Notizen werden in Ansible Playbook genannt.

---
## First Ansible Playbook
#### Create a Message of the Day

```YAML
# My firstone.yml
- name: Install an Apache Package
  hosts: clients
  connection: local
  become: yes
  tasks:
    - name: Create Message of the Day
      lineinfile:
        path: /etc/motd
        line: 'Managed by Ansible {{ ansible_hostname }}'
        create: yes
```

---
## First Ansible Playbook
#### Install a APT Package

```YAML
  # My playbook-1.yml
  # https://gitlab.com/snippets/1773188
  - name: Install an Apache Package
    hosts: clients
    become: yes
    tasks:
      - name: Install apache2
        tags: apache2
        apt:
          name: apache2
          state: present
```
---
# Starten eines Service

```YAML
  # My playbook-2.yml
  # https://gitlab.com/snippets/1773189
  - name: Install an Apache Package
    hosts: clients
    become: yes
    tasks:
      - name: Install apache2
        tags: apache2
        apt:
          name: apache2
          state: present

      - name: Start Apache
        tags: apache-service
        service:
          name: apache2
          state: started

```

---
## Ansible Playbook Execute Optionen

* Simple Playbook Run
```
ansible-playbook -i /vlab-files/ansible-inventory.yml /vlab-files/first-playbook.yml
```
* Ansible Diff Mode
```
ansible-playbook -i /vlab-files/ansible-inventory.yml /vlab-files/first-playbook.yml --diff
```

* Ansible Check Mode
```
ansible-playbook -i /vlab-files/ansible-inventory.yml /vlab-files/first-playbook.yml --diff --check
```

---
## Ansible Playbook Execute Optionen

* Nur bestimte Tasks ausführen
```
ansible-playbook -i /vlab-files/ansible-inventory.yml /vlab-files/first-playbook.yml --tags apache2
```
* Nur eine bestimmte Hostgruppe ausführen
```
ansible-playbook -i /vlab-files/ansible-inventory.yml /vlab-files/first-playbook.yml --limit client.local
```

---
## A LAMP Environment with a Playbook
```YAML
# YOUR Playbook
```

---
## Aufgaben Ansible
#### Level 1

Installiere einen Web Application Stack mit einer Aplikation wie in der letzten Übung , aber diesmal mit einem Ansible Playbook.

##### Beispiel:
* LAMP
	* Kanboard (https://kanboard.org/)

---
## Ansible Rollen
#### Um in Ansible Playbooks wiederverwenden zu können und so auch Komplexere Software Installieren zu können. Gibt es in Ansible sogenannte Rollen.

---
## Eigenschaften von Rollen in Ansible

---
## Ansible Rollen ein Überblick
![Architektur Rollen](_images/ansible-architektur-rollen.png)

---
## Recap Hostfile
```YAML
# invnentory.yaml
all:
  hosts:
    localhost:
  children:
    webserver:
      hosts:
        client.local:
          ansible_host: client.local
```

---
## Changes on Playbook
```YAML
- hosts: webserver
  gather_facts: false
  roles:
    - ansible-role-testing-example
```

---
## Erstellen einer Rolle
Erstellen eines Verzeichnisses für die Rolle
* mkdir ansible-role-testing-example
* mkdir tasks
  * touch main.yml
---
## Basic Example Role

```YAML
# main.yml
- name: Create Message of the Day
  copy:
    content: 'Managed by Ansible'
    dest: '/etc/motd'
```

---
## Testen von Rollen
### Um die Qualität seiner Rolle zu verbessern , kann diese in einer Virtuellen Umgebung getestet werden.

---
### Um das Testen der Rolle schneller und Reproduzierbar zu machen kann in der Rolle ein Playbook für diese Tests erstellt werden.

---
## Tests für die Rolle
* mkdir tests
* touch test.yml
---
## test.yml
```YAML
- hosts: all
  gather_facts: true
  roles:
    - { role: '../../ansible-role-testing-example' }
```

---
## Testen von Rollen
### (Methoden)
* Manuel
* Vagrant
* Docker
* Continious Integration

---
## Testen von Rollen
### (Manuell)
* Installation einer Virtualisierungsumgebung (Virtualbox)
---
## Installation Vagrant
* Installation einer Configurationsmanagemnet Umgebung (Vagrant)

---
## Initialisierung des Basis Betriebssystems
### Vagrant Basis Box
* mkdir vagrant
* vagrant init
* vi Vagrantfile
---
## Vagrantfile (Example)
```RUBY
Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial64"
end
```
---
## Starten der Virtuellen Umgebung
* cd tests
* cd vagrant
* vagrant up
* vagrant ssh
---
## Installation von Ansible
* sudo apt-get install -y software-properties-common
* sudo apt-add-repository ppa:ansible/ansible
* sudo apt-get update
* sudo apt-get install -y ansible
---
## Clonen der Rolle in die Testumgebung
git clone https://gitlab.com/rstumpner/ansible-role-testing-example

---
## Ausführen der Tests
#### Build Config Files
* ansible-playbook -i host test/test.yml --tags=build

---
## Ausführen der Tests
#### Stelle den Unterschied zum Testsystem fest
* ansible-playbook -i host test/test.yml --diff

---
## Ausführen der Tests
#### Führe die Tests aus verändere aber nicht das Testsystem
* ansible-playbook -i host test/test.yml -check-mode

---
## Ausführen der Tests
#### Führe die Tests in der Virtuellen Umgebung aus
* ansible-playbook -i host test/test.yml

---
## Ansible Role Testing
### (with Vagrant)
* Installation von Virtualbox (https://www.virtualbox.org/)
  * apt-get install virtualbox
* Installation Vagrant (https://www.vagrantup.com/)
  * apt-get install vagrant

---
## Ansible Rollen Tests
* Erstelle ein Verzeichnis für die Tests der Rolle

  ``` mkdir tests/vagrant/ ```

* Erstelle ein Playbook für die Tests

  ``` touch test.yml ```

---
## test.yml

```YAML
# Ansible Role Testing
# https://gitlab.com/snippets/1773192
- hosts: all
  gather_facts: true
  roles:
    - { role: '../../ansible-role-testing-example' }

```
---
## Vagrantfile
#### Erstelle ein Vagrant File mit einem Ansible Provisioner und den Rollen Tests (test.yml)

```RUBY
# https://gitlab.com/snippets/1773190
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure('2') do |config|
  config.vm.define "client" do |client|
    client.vm.box = "ubuntu/xenial64"
    client.vm.hostname = 'ansible-role-testing'
    client.vm.network "private_network", type: "dhcp"
    client.vm.network "forwarded_port", guest: 80, host: 8082
    client.vm.provision "ansible" do |ansible|
            ansible.playbook = "../test.yml"
            ansible.verbose = "vvv"
    end
  end
end
```
---
## Clone Ansible Example Role

git clone https://gitlab.com/rstumpner/ansible-role-testing-example

---
## Testing with Vagrant

* cd tests/vagrant/
* vagrant up

---
## Debug der Test Umgebung

* vagrant ssh

---
## Retesting with Vagrant

* vagrant provision

---
## Role Testing with Docker
### Um schneller Testen zu können ist es auch möglich die Umgebung zum Testen mit Docker zu erzeugen.

---
## Installation Docker
* curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
* sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
* sudo apt-get update
* sudo apt-get install docker-ce
---
## Erstellen eines neuen Tests
* mkdir  ansible-docker

---
## Erstellen eines neuen Tests
* touch ansible-docker-test.yml

---
## ansible-docker-test.yml (Step 1)
#### Suche ein Dockerfile im Verzeichnis und baue ein Docker Image daraus . Wenn nicht vorhanden suche den Image Namen auf Dockerhub.

---

```YAML
vars:
  image_name: "ubuntu:16.04"
tasks:  
  - name: Build or pull image if needed
    docker_image:
      name: "{{ image_name.split(':')[0] }}"
      tag: "{{ image_name.split(':')[1] }}"
      dockerfile: "Dockerfile.{{ image_name | replace(':', '.') }}"
      path: "{{ 'images' if 'builded' in image_name else '' }}"
      force: "{{ force_build_image | default(false) }}"
```
---
## Starte einen Docker Container mit dem Image
```YAML
    - name: Run docker for testing Ansible role
      docker_container:
        name: "ansible-role-test"
        image: "{{ image_name }}"
        command: "/sbin/init"
        state: started
        privileged: true

```
---
## Füge den Docker Container in das Ansible Host Inventory hinzu
```YAML
    - name: Add Dockers into ansible-role-test inventory group
      add_host:
        name: "ansible-role-test"
        ansible_connection: docker
        ansible_user: root
        ansible_python_interpreter: python
        groups: ansible-role-test
      changed_when: false
```

---
## Führe die Rolle in dem gerade Erzeugten Docker Conteiner aus.

```YAML
- hosts: ansible-role-test
  gather_facts: false
  vars:
    ansible_python_interpreter: /usr/bin/python3
  pre_tasks:
    - name: install python 3
      raw: test -e /usr/bin/python3 || (apt -y update && apt install -y python3-minimal)  
  roles:
    - { role: '../../../ansible-role-testing-example' }
```

---
## Mit Docker kann nun die Rolle sehr einfach gegen verschiedene Distributionen getestet werden

---
## Es wird nun schon Kompliziert die Verschiedenen Versionen von der Ansible Rolle und dem Testergebnis zu unterscheiden und zu Dokumentieren.

---
## Die Lösung dazu ist unsere Rolle in eine Sourcecodeversionierung einzupflegen und bei jedem Update einen Test automatiesiert auszuführen.
---
## Continuous integration (Überblick)
![Continuous integration](_images/ansible-architektur-ci.png)

---
## .gitlab-ci.yml (Stages)

```YAML
stages:
  - review
  - testing


```

---
## .gitlab-ci.yml (Variablen)

```YAML
variables:
  ANSIBLE_FORCE_COLOR: 'true'
  GIT_SSL_NO_VERIFY: "1"
  ANSIBLE_HOST_KEY_CHECKING: 'false'

```

---
## .gitlab-ci.yml (Stage)

```YAML
syntax-check:
  stage: review
  image: ubuntu:latest
#  only:
#    - review
#  tags:
#    - test
  before_script:
    - apt-get update -qq
    - apt install --no-install-recommends -y python-minimal software-properties-common git vim iputils-ping mtr dnsutils rsync tree python-pip
    - apt-add-repository ppa:ansible/ansible
    - apt update
    - apt install -y ansible python-markupsafe python-ecdsa libyaml-0-2 python-jinja2 python-yaml python-paramiko python-httplib2 python-crypto sshpass
    - pip install pysphere
    - pip install pyvmomi
  script:
    - 'ansible-playbook -i tests/ansible-docker/hosts tests/test.yml --syntax-check'
```
---
---
## Aufgaben Ansible
#### Level 2

Erstelle eine Rolle für einen Web Application Stack mit einer Aplikation

##### Beispiel:
* LAMP
	* Kanboard (https://kanboard.org/)

Erstelle einen Test für diese Rolle

##### Beispiel:
* LAMP
	* Kanboard (https://kanboard.org/)

---
## Ansible Cheat Sheet
* Ubuntu 16.04 Python Error:
  ansible --inventory-file=/vlab-files/ansible-inventory client.local -m ping -u ansible -k -e 'ansible_python_interpreter=/usr/bin/python3'
---
# Links
* Ansible Module Index
  http://docs.ansible.com/ansible/latest/modules/modules_by_category.html
* Ansible Playbooks best Practice
  https://www.ansible.com/blog/make-your-ansible-playbooks-flexible-maintainable-and-scalable
---
# Begriffe
* Continuous integration
  https://en.wikipedia.org/wiki/Continuous_integration
  https://de.wikipedia.org/wiki/Kontinuierliche_Integration
